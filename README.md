# Caffe v1.0.0


# Installation

## System 
* cmake > 2.8
* gcc >= 4.9
* boost

```Bash
sudo apt-get install libprotobuf-dev libleveldb-dev libsnappy-dev libopencv-dev libhdf5-serial-dev protobuf-compiler
sudo apt-get install --no-install-recommends libboost-all-dev
sudo apt-get install libgflags-dev libgoogle-glog-dev liblmdb-dev
```

## Python
python 2.7 

* scikit-image

> Tips: Error may be raised within the installation of networkx, while pip install scikit-image. Download the whl, change required information in `*info/META*`, then: `pip install networkx==2.2 && pip install scik*.whl` 

## With python interface
```sh
mkdir build
cd build
cmake ..
make -j
```

# test
```
cd python
python -c "import caffe; print(caffe.__path__)"
```
